let courses = [
	{
		id: "1",
		name: "HTML",
		description: "Website Structure",
		price: 123,
		isActive: true
	},
	{
		id: "2",
		name: "CSS",
		description: "Website Design",
		price: 456,
		isActive: false
	},
	{
		id: "3",
		name: "JAVASCRIPT",
		description: "Website Interaction",
		price: 678,
		isActive: true
	},
	{
		id: "4",
		name: "PYTHON",
		description: "Programming language",
		price: 897,
		isActive: true
	}
]
//1. Create
const addCourse = (newCourse) => {
	courses.push(newCourse);
	alert(`You have created ${newCourse.name}. Its price is ${newCourse.price}`)
}
addCourse({id: "6", name: "newCourse", description: "New description", price: 2500, isActive: true})
console.log(courses)
//2. Retrieve/Read
const getSingleCouse =(id) => {
	let foundCourse = courses.find(function(course){
		return course.id === id
	})
	console.log(foundCourse)
}
getSingleCouse("2")

const getAllCourses = courses.forEach(element =>{
	console.log(element)
	return element
})
//3. Updat(e
const archiveCourse = (num) => {
	courses[num].isActive = false
}
archiveCourse(0)
console.log(courses)
//4. Delete
const deleteCourse = () => {
	courses.pop()
}
deleteCourse()
console.log(courses)
//5. Stretch goal
const filterCourse = courses.filter(element => element.isActive === true)
console.log(filterCourse)